from robco import *

def GrelokNew():
    inventory = ['Drinking Flask', 'Rusty Sword']
    grelokdata = {
        'var1':False, #True when gemstone taken
        'var2':False, #True when drinking flask filled
        'var3':False, #True when zombie encountered (looked around at Chapel)
        'var4':False, #True when zombie slain
        'var5':False, #True when zombie head taken
        'var6':False, #True when Gemstone cut in half
        'var7':False, #True when sword tuned
        'var8':False, #True when chapel key obtained
        'var9':False, #True when grelok slain
        'var10':False, #True when looked around at Mountainside
        'var11':False, #True when looked around in Town
        'var12':False, #True when looked around at Swamp
        'var13':False, #True when chapel's door opened
        'var14':False #True  when you don't have gemstone AND talked to wizard at least once
    }
    ClearScreen()
    Header()
    print('Reign of Grelok (beta v.632)')
    print('-'*28)
    print('\nThis software is still in early beta and does not represent the final state of the game.\nPress ENTER to start. Enter \'q\' at any moment to quit.')
    input()
    GrelokPlains(inventory, grelokdata)

def GrelokPlains(inventory, grelokdata):
    while True:
        ClearScreen()
        Header()
        print('Reign of Grelok (beta v.632)')
        print('-'*28)
        print('(Plains) ')
        action = input('[L]ook/[I]nventory/N/S/E/W>').lower()
        if action == 'l':
            print('You are standing in a wide plain.  Foothills stretch to the north, ')
            print('where clouds gather around an ominous peak. ')
            print('A dirt path winds from a lonely chapel to the east, through the plains ')
            print('where you\'re standing, and south into a bustling town.' )
            print('Wispy mists gather over marshland in the west,')
            print('where a thin tower stands alone in the bog.')
            input()
        elif action == 'i':
            GrelokInventory(inventory, grelokdata)
        elif action == 'n':
            GrelokMountain(inventory, grelokdata)
        elif action == 's':
            GrelokTown(inventory, grelokdata)
        elif action == 'e':
            GrelokChapel(inventory, grelokdata)
        elif action == 'w':
            GrelokSwamp(inventory, grelokdata)
        elif action == 'q':
            Menu().cmdloop()
            
def GrelokMountain(inventory, grelokdata):
    while True:
        ClearScreen()
        ClearScreen()
        Header()
        print('Reign of Grelok (beta v.632)')
        print('-'*28)
        if grelokdata['var10'] == False:
            print('(Mountainside) ')
            action = input('[L]ook/[I]nventory/S>').lower()
            if action == 'l':
                print('You are on the craggy, windblasted face of a mountain. ')
                print('Stormclouds coil above the summit, pelting you and the sparse')
                print('vegetation with torrential downpour.  Far below, beyond the foothills, ')
                print('a wide plain stretches across the southern horizon.')
                print('\n')
                print('Grelok is here, spewing heresies.')
                print('\n')
                print('A glint between the rocks catches your eye.')
                grelokdata['var10'] = True
                input()
            elif action == 'i':
                GrelokInventory(inventory, grelokdata)
            elif action == 's':
                GrelokPlains(inventory, grelokdata)
            elif action == 'q':
                Menu().cmdloop()
        elif grelokdata['var1'] == False:
            ClearScreen()
            Header()
            print('Reign of Grelok (beta v.632)')
            print('-'*28)
            print('(Mountainside)')
            action = input('[L]ook/[I]nventory/Investi[g]ate object/[U]se sword Grelok/S>').lower()
            if action == 'l':
                print('You are on the craggy, windblasted face of a mountain. ')
                print('Stormclouds coil above the summit, pelting you and the sparse')
                print('vegetation with torrential downpour.  Far below, beyond the foothills, ')
                print('a wide plain stretches across the southern horizon.')
                print('\n')
                print('Grelok is here, spewing heresies.')
                if grelokdata['var1'] == False:
                    print
                    print('A glint between the rocks catches your eye.')
            elif action == 'i':
                GrelokInventory(inventory, grelokdata)
            elif action == 'g':
                print('You take a rough gemstone from the rocks.')
                grelokdata['var1'] = True
                inventory.append('Raw gemstone')
                input()
            elif action == 's':
                GrelokPlains(inventory, grelokdata)
            elif action == 'u':
                print('Your puny weapons are useless on Grelok.')
                input()
            elif action == 'q':
                Menu().cmdloop()
        elif (grelokdata['var1'] == True) and (grelokdata['var7'] == False):
            ClearScreen()
            Header()
            print('Reign of Grelok (beta v.632)')
            print('-'*28)
            print('(Mountainside)')
            action = input('[L]ook/[I]nventory/[U]se sword Grelok/S>').lower()
            if action == 'l':
                print('You are on the craggy, windblasted face of a mountain. ')
                print('Stormclouds coil above the summit, pelting you and the sparse')
                print('vegetation with torrential downpour.  Far below, beyond the foothills, ')
                print('a wide plain stretches across the southern horizon.')
                print('\n')
                print('Grelok is here, spewing heresies.')
                if grelokdata['var1'] == False:
                    print('\n')
                    print('A glint between the rocks catches your eye.')
            elif action == 'i':
                GrelokInventory(inventory, grelokdata)
            elif action == 's':
                GrelokPlains(inventory, grelokdata)
            elif action == 'u':
                print('Your puny weapons are useless on Grelok.')
            elif action == 'q':
                Menu().cmdloop()
        elif grelokdata['var7'] == True:
            ClearScreen()
            Header()
            print('Reign of Grelok (beta v.632)')
            print('-'*28)
            print('(Mountainside)')
            action = input('[L]ook/[I]nventory/[U]se magic sword Grelok/S>').lower()
            if action == 'l':
                print('You are on the craggy, windblasted face of a mountain. ')
                print('Stormclouds coil above the summit, pelting you and the sparse')
                print('vegetation with torrential downpour.  Far below, beyond the foothills, ')
                print('a wide plain stretches across the southern horizon.')
                print('\n')
                print('Grelok is here, spewing heresies.')
                if grelokdata['var1'] == False:
                    print('\n')
                    print('A glint between the rocks catches your eye.')
            elif action == 'i':
                GrelokInventory(inventory, grelokdata)
            elif action == 's':
                GrelokPlains(inventory, grelokdata)
            elif action == 'u':
                print('When you draw your sword, Grelok lowers his great horned head andbellows ')
                print('laughter in your face.  You grit your teeth and swing a mighty two-handed blow,')
                print('the magical blade ringing clearly, even amid the tumult of throaty cackling.  ')
                print('\n')
                print('You swing the sword so fiercely, it escapes your grip and hurtles into')
                print('the open maw of the monstrosity, lost from sight ')
                print('in the arid darkness of Grelok\'s throat.' )
                print( 'You step back as Grelok jerks his mouth shut and stands upright.  ')
                print( 'He is still for a moment, then starts clawing at his neck.  ')
                print( 'Muffled, a ringing can be heard as if from a great distance.')
                print('\n')
                print('Suddenly, Grelok\'s chest bursts in a fount of viscous, green blood.' )
                print('The Ringing can be heard clearly now, and as thick lifeblood oozes around ')
                print('the protruding tip of the magic sword, ')
                print('the stormclouds swirling the peak are already clearing.  Grelok is defeated!')
                print('\n')
                print('THE END')
                print('(Thanks for playing!)')
                grelokdata['var9'] = True
                input()
                Menu().cmdloop()
            elif action == 'q':
                Menu().cmdloop()
                 
def GrelokTown(inventory, grelokdata):
    while True:
        ClearScreen()
        if grelokdata['var11'] == False:
            ClearScreen()
            Header()
            print('Reign of Grelok (beta v.632)')
            print('-'*28)
            print('(Town)')
            action = input('[L]ook/[I]nventory/N>').lower()
            if action == 'l':
                print('You\'re standing in the dusty market square of a quiet town.')
                print('Many of the shops and homes lie abandoned, and the citzens that can be seen')
                print('speak in hushed voices, casting furtive glances at the darkened skyline in')
                print('the distant north.  The ringing of an anvil breaks the silence regularly, ')
                print('where a mustachioed blacksmith bends over his work in a nearby tent.')
                print('\n')
                print('The blacksmith is here, working.')
                print('A priest is here, drinking.')
                grelokdata['var11'] = True
                input()
            elif action == 'i':
                GrelokInventory(inventory, grelokdata)
            elif action == 'n':
                GrelokPlains(inventory, grelokdata)
            elif action == 'q':
                Menu().cmdloop()
        else:
            ClearScreen()
            Header()
            print('Reign of Grelok (beta v.632)')
            print('-'*28)
            print('(Town)')
            action = input('[L]ook/[I]nventory/Speak with [B]lacksmith/Speak with [P]riest/N>').lower()
            if action == 'n':
                GrelokPlains(inventory, grelokdata)
            elif action == 'i':
                GrelokInventory(inventory, grelokdata)
            elif action == 'b':
                if grelokdata['var6'] == False:
                    print('Your eyes water from the smoke and smarmy heat inside the tent.  ')
                    print('The huge man swipes sweat from his bald head and looks up from his work.')
                    print('\n')
                    print('"There\'s no shortage of work to be done with Grelok scarin\' everyone witless.' )
                    print('Leave me to filling my orders, stranger."  With that, the blacksmith dismisses ')
                    print('you from his tent and douses a hot blade in water, hissing with steam.')
                    input()
                else:
                    print('The blacksmith regards you gruffly and is about to dismiss you when you produce')
                    print('the polished gemstone from your bag.  He sets his hammer aside and twirls')
                    print('his moustache.')
                    print('\n')
                    print('"A right fine stone, that is." He says, admiring the faceted stone, ')
                    print('"What would you be needin'', then?"')
                    print('\n')
                    print('Following your careful instructions, the smithy re-forges your rusty sword ')
                    print('with the magical shard at the center of the blade.')
                    inventory.append('Magic Sword')
                    inventory.remove('Rusty Sword')
                    inventory.remove('Refined Gemstone')
                    inventory.remove('Magical Shard')
                    grelokdata['var7'] = True
                    input()
            elif action == 'p':
                if grelokdata['var4'] == False: #zombie ain't killed yet
                    print('The priest notices your approach and looks up from his swilling.')
                    print('"Grelok is come, and we are forsaken!", he cries.  "Urp!", he continues.')
                    print('\n')
                    print('As you recover from the stench of the priestly belch, you are told that ')
                    print('the priest has fled from his nearby chapel.  When Grelok arrived')
                    print('on the mountain, the dead in his cemetery began to rise, and his ')
                    print('congregation scattered. ')
                    print('\n')
                    print('"If you could rid the place of the zombies", he tells you, ')
                    print('"I\'ll give you the key, and you can help yourself to the apothecary"')
                    input()
                elif (grelokdata['var5'] == True) and (grelokdata['var8'] == False):
                    print('The priest drunkenly curses the undead who have defiled his church.' )
                    print('You present him with the decapitated zombie head from your bag.')
                    print('\n')
                    print('"Praise you!", he hiccups.  "Perhaps Grelok\'s influence isn\'t')
                    print('so strong!".  With that, he turns his decanter over on the head and ')
                    print('tosses into a fireplace, where it bursts into purple flame and ')
                    print('burns up almost instantly.')
                    print('\n')
                    print('"I must gather the faithful."  He presses a brass key into your palm,')
                    print('"Please, help yourself to what little may be of use at my chapel."')
                    inventory.append('Brass key')
                    grelokdata['var8'] = True
                    input()
                elif grelokdata['var8'] == True:
                    print('The priest is drinking water, poring over a thick, leatherbound volume')
                    print('connected by a thick leather thong to his neck.  He notices you only ')
                    print('when you\'ve come very close.')
                    print('\n')
                    print('"Ah, good friend!  Have you gone ahead to open the chapel?')
                    print('My body still aches with drink, I\'m afraid, but soon ')
                    print('I will gather the congregation and return myself."')
                    input()
            elif action == 'q':
                Menu().cmdloop()
                
def GrelokChapel(inventory, grelokdata):
    while True:
        ClearScreen()
        Header()
        print('Reign of Grelok (beta v.632)')
        print('-'*28)
        if grelokdata['var3'] == False:
            print('(Chapel)')
            action = input('[L]ook/[I]nventory/W>').lower()
            if action == 'l':
                print('You stand at the end of a dirt path, facing a small chapel.' )
                print('The stucco walls are faded, many roof tiles are missing.' )
                print('The great oaken doors are locked.  The congregation is nowhere to be found.') 
                print('A small cemetery of crooked headstones lies in the shadow of the cracked steeple' )
                print('The dirt path winds westward through a great, featureless plain.')
                print('\n')
                print('A zombie totters aimlessly nearby.')
                print('\n')
                print('There is an open grave nearby.')
                grelokdata['var3'] = True
                input()
            elif action == 'i':
                GrelokInventory(inventory, grelokdata)
            elif action == 'w':
                GrelokPlains(inventory, grelokdata)
            elif action == 'q':
                Menu().cmdloop()
        elif (grelokdata['var3'] == True) and (grelokdata['var4'] == False):
            print('(Chapel)')
            action = input('[L]ook/[I]nventory/[U]se sword zombie/[E]xamine grave/W>').lower()
            if action == 'w':
                GrelokPlains(inventory, grelokdata)
            elif action == 'i':
                GrelokInventory(inventory, grelokdata)
            elif action == 'e':
                print('There is a deep, empty grave in the cemetery.' )
                print('Several bloated rats floating in a foot of filthy water at the bottom.' )
                print('Don\'t fall in!')
                input()
            elif action == 'u':
                print('Your blow knocks the zombie into a grave.')
                grelokdata['var4'] = True
                input()
            elif action == 'q':
                Menu().cmdloop()
        elif (grelokdata['var4'] == True) and (grelokdata['var13'] == False):
            print('(Chapel)')
            action = input('[L]ook/[I]nventory/[E]xamine grave/W>').lower()
            if action == 'i':
                GrelokInventory(inventory, grelokdata)
            elif action == 'w':
                GrelokPlains(inventory, grelokdata)
            elif action == 'l':
                print('You stand at the end of a dirt path, facing a small chapel.  The stucco')
                print('walls are faded, many roof tiles are missing.  The great oaken doors are locked.' )
                print('The congregation is nowhere to be found.  A small cemetery of crooked headstones')
                print('lies in the shadow of the cracked steeple.  The dirt path winds westward through')
                print( '1a great, featureless plain.')
                print('\n')
                print('There is an open grave nearby.')
                input()
            elif action == 'e':
                print('There is a deep, empty grave in the cemetery.  Several bloated rats and')
                print('a zombie corpsefloat in a foot of filthy water at the bottom.  Don\'t fall in!')
                print('\n')
                print('A grotesque zombie head is stuck on a root near the top of the grave.' )
                print('You bag the horrific trophy as proof of your deed.')
                inventory.append('Zombie head')
                grelokdata['var5'] = True
            elif action == 'q':
                Menu().cmdloop()
        elif grelokdata['var8'] == True:
            print('(Chapel)')
            action = input('[L]ook/[I]nventory/[E]xamine grave/Examine [C]apel/W>').lower()
            if action == 'l':
                if grelokdata['var13'] == True:
                    print('You stand at the end of a dirt path, facing a small chapel.  The stucco')
                    print('walls are faded, many roof tiles are missing.  The great oaken doors are unlocked.' )
                    print('The congregation is nowhere to be found.  A small cemetery of crooked headstones')
                    print('lies in the shadow of the cracked steeple.  The dirt path winds westward through')
                    print('a great, featureless plain.')
                    print('\n')
                    print('The chapel doors are unlocked.')
                    print('There is an open grave nearby.')
                    input()
                else:
                    print('You stand at the end of a dirt path, facing a small chapel.  The stucco')
                    print('walls are faded, many roof tiles are missing.  The great oaken doors are unlocked.' )
                    print('The congregation is nowhere to be found.  A small cemetery of crooked headstones')
                    print('lies in the shadow of the cracked steeple.  The dirt path winds westward through')
                    print('a great, featureless plain.')
                    print('\n')
                    print('There is an open grave nearby.')
                    input()
            elif action == 'i':
                GrelokInventory(inventory, grelokdata)
            elif action == 'w':
                GrelokPlains(inventory, grelokdata)
            elif action == 'e':
                print('There is a deep, empty grave in the cemetery.  Several bloated rats and')
                print('a zombie corpsefloat in a foot of filthy water at the bottom.  Don\'t fall in!')
                input()
            elif action == 'c':
                print('Dust motes hang lazily in the shafts of colored light stretching across the chapel')
                print('from peaked windows.  The pews, pulpit, and everything else are covered in a fine mist.' )
                print('There is a very deep stone cistern near the entrance.' )
                print('It is full to the brim with blessed water.')
                print('\n')
                print('There is more than enough water here to fill your tiny flask.')
                grelokdata['var2'] = True
                grelokdata['var13'] = True
                input()
            elif action == 'q':
                Menu().cmdloop()
                
def GrelokSwamp(inventory, grelokdata):
    while True:
        ClearScreen()
        Header()
        print('Reign of Grelok (beta v.632)')
        print('-'*28)
        if grelokdata['var12'] == False:
            print('(Swamp)')
            action = input('[L]ook/[I]nventory/E>').lower()
            if action == 'l':
                print('You are standing on a narrow stone path in a dark marsh.' )
                print('Greasy bubbles float to the top of the bog-waters on either side and pop lazily,')
                print('spattering your legs with muck and slime.  A short, stone tower squats here.' )
                print('No door is visible, and the stones are smooth and polished.' )
                print('A balcony juts out midway up the tower\'s face.  The heady smells of incense')
                print('mix with the nauseating stench of the swamp.  The stone path unfurls eastward,')
                print('towards a broad plain beyond the marshes.')
                print('\n')
                print('A wizard is here, gesticulating wildly from his balcony.')
                grelokdata['var12'] = True
                input()
            elif action == 'i':
                GrelokInventory(inventory, grelokdata)
            elif action == 'e':
                GrelokPlains(inventory, grelokdata)
            elif action == 'q':
                Menu().cmdloop()
        elif grelokdata['var1'] == False:
            print('(Swamp)')
            action = input('[L]ook/[I]nventory/[T]alk to the wizard/E>').lower()
            if action == 'l':
                print('You are standing on a narrow stone path in a dark marsh.' )
                print('Greasy bubbles float to the top of the bog-waters on either side and pop lazily,')
                print('spattering your legs with muck and slime.  A short, stone tower squats here.' )
                print('No door is visible, and the stones are smooth and polished.' )
                print('A balcony juts out midway up the tower\'s face.  The heady smells of incense')
                print('mix with the nauseating stench of the swamp.  The stone path unfurls eastward,')
                print('towards a broad plain beyond the marshes.')
                print('\n')
                print('A wizard is here, gesticulating wildly from his balcony.')
                input()
            elif action == 'i':
                GrelokInventory(inventory, grelokdata)
            elif action == 'e':
                GrelokPlains(inventory, grelokdata)
            elif action == 't':
                if grelokdata['var14'] == False:
                    print('The wizard beckons wildly at you from his balcony.  "You\'re here,')
                    print('you\'ve arrived!", he exclaims.  After an awkward silence, he jabs')
                    print('an excited finger into a crystal ball, nearly knocking it into the bog.' )
                    print('\n')
                    print('"I\'ve seen, you see.  You\'re the one to defeat Grelok.  Hoo-hoo!"')
                    print('The little man hops onto the railing, spinning a pirouette.')
                    print('"Now the time\'s come to play my part.  Toss up the gem!"')
                    print('\n')
                    print('The wizard\'s brow furrows.  "Got things a bit out of order, have I?' )
                    print('Come back when you\'ve got a powerful gemstone.' )
                    print('Soon - I\'ve never got to fulfill a prophecy before!"')
                    grelokdata['var14'] = True
                    input()
                else:
                    print('The wizard is shooing you away, his sleeves flopping about.')
                    print('\n')
                    print('"Go!  Find the gemstone and return, so I can play my part!"')
                    input()
            elif action == 'q':
                Menu().cmdloop()
        elif grelokdata['var1'] == True:
            print('(Swamp)')
            action = input('[L]ook/[I]nventory/[T]alk to the wizard/E>').lower()
            if action == 'l':
                print('You are standing on a narrow stone path in a dark marsh.' )
                print('Greasy bubbles float to the top of the bog-waters on either side and pop lazily,')
                print('spattering your legs with muck and slime.  A short, stone tower squats here.' )
                print('No door is visible, and the stones are smooth and polished.' )
                print('A balcony juts out midway up the tower\'s face.  The heady smells of incense')
                print('mix with the nauseating stench of the swamp.  The stone path unfurls eastward,')
                print('towards a broad plain beyond the marshes.')
                print('\n')
                print('A wizard is here, gesticulating wildly from his balcony.')
                input()
            elif action == 'i':
                GrelokInventory(inventory, grelokdata)
            elif action == 'e':
                GrelokPlains(inventory, grelokdata)
            elif action == 't':
                if grelokdata['var6'] == False:
                    print('"Hoo-hoo! The slayer of Grelok approaches, raw stone in hand,')
                    print('just as I\'ve seen!" The wizard\'s pointy hat bobs excitedly as he')
                    print('points a finger at you.  Suddenly, a pale orange arc of light extends from the')
                    print('knobby finger and draws the gemstone from your bag before you can react.' )
                    print('The gemstone halts and hovers in the air before the wizard\'s nose.')
                    print('"Essence be true, powers renew, Fatty-Hoo-Do!"  With that, ')
                    print('he slaps the hovering stone, smashing it against the smooth stone of the tower.') 
                    print('In a burst of light, the stone splits into two, and one lands in each')
                    print('outstretched palm of the hopping little wizard.')
                    print('\n')
                    print('"Shard for the sword. Wrap her in iron and she\'ll find Grelok\'s black ')
                    print('heart for you. Take the chaff, too. You\'ll need payment for a smith to forge')
                    print('the weapon."  He tosses the stones down which you leap forward to catch safely.')
                    inventory.remove('Raw gemstone')
                    inventory.append('Refined Gemstone')
                    inventory.append('Magical Shard')
                    grelokdata['var6'] = True
                    input()
                else:
                    print('"Get you to a smithy!  Forge the shard with sword, and defeat Grelok!"')
                    print('\n')
                    print('The wizard tosses some pebbles down to shoo you away and busies himself ')
                    print('conjuring colored puffs of smoke.')
                    input()
            elif action == 'q':
                Menu().cmdloop()
                
def GrelokInventory(inventory, grelokdata):
    ClearScreen()
    Header()
    print('Reign of Grelok (beta v.632)')
    print('-'*28)
    print('Your inventory')
    for i in inventory:
        print(i)
    input()