# What is this? #
**pyRobco3** is a small program that "simulates" the terminals that can be found in Fallout 3 and Fallout New Vegas.

## What does work? ##
There is a crude and simple line editor which serves as a Journal-It software.
There also is Reign of Grelok text game (Fallout 3 easter egg).

## What is different from the original? ##
The interface is a typical console, so there are no buttons to press, you just have to type in commands. I believe like this is how those terminals would work if they were not designed to be game-friendly.
