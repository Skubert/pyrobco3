#!/usr/bin/env python3

#
#   Written for Python 3.5.1
#
#   Compatible with Windows, Linux, BSD and Mac OS X. 
#   Other OS's may need a change in ClearScreen() function
#
#               pyRobco3
#       Fallout Terminal Simulator
#

import time, sys, os, cmd, textwrap, time, datetime
import grelok

#=========================================

__author__ = 'Piotr "Skubert" Skubała'
__version__ = 'v.16b'
__license__ = 'Beerware (Rev. 42)'

#=========================================

currentPrompt = ''
currentUser = ''
directoryRobco = os.path.dirname(os.path.realpath(__file__)) + '/disk'
currentTermlink = True

def newConfig():
    ClearScreen()
    print('  WELCOME TO ROBCO INDUSTRIES (TM) TERMLINK\n')
    DelayedPrint('  ERROR! CONFIGURATION CORRUPTED!', 0.06)
    DelayedPrint('  RESTORE BASIC CONFIGURATION? WARNING: OLD SETTINGS WILL BE OVERWRITTEN', 0.06)
    while True:
        choice = input('Y/N>')
        if choice.upper() == 'Y':
            if(os.path.exists(directoryRobco)) == False:
                os.mkdir(directoryRobco)
            handle = open(directoryRobco + '/settings.ini', 'w')
            handle.write('> \n')
            handle.write('%USER%\n')
            handle.write('True')
            handle.close()
            break
            
        elif choice.upper() == 'N':
            print('\n')
            DelayedPrint('  CONFIGURATION CORRUPTED. CONSULT ADMINISTRATOR.', 0.06)
            input()
            quit()

def readConfig():
    settings = []
    try:
        handle = open(directoryRobco + '/settings.ini', 'r')
        #settings = handle.read().splitlines()
        settings.append(handle.readline())
        settings.append(handle.readline().rstrip())
        if handle.readline() == 'True':
            settings.append(True)
        else:
            settings.append(False)
        handle.close()
    except FileNotFoundError:
        newConfig()
        settings.append('> ')
        settings.append('%USER%')
        settings.append(True)
    return settings

def writeConfig():
    handle = open(directoryRobco + '/settings.ini', 'w')
    handle.write(currentPrompt + '\n')
    handle.write(currentUser + '\n')
    handle.write(str(currentTermlink))
    handle.close()

def DelayedPrint(input, timer):
    for i in range(0, len(input)):
        sys.stdout.write(input[i])
        sys.stdout.flush()
        time.sleep(timer)
    print('\n')

def ClearScreen():
    if os.name == 'nt':
        os.system('cls')
    elif os.name == 'posix':
        os.system('clear')
        
def Header():
    print('ROBCO INDUSTRIES UNIFIED OPERATING SYSTEM'.center(80))
    print('COPYRIGHT 2075-2077 ROBCO INDUSTRIES'.center(80))
    print('-LOCAL MODE-'.center(80))

def termlinkTest():
    robco.ClearScreen()
    print('  WELCOME TO ROBCO INDUSTRIES (TM) TERMLINK\n')
    print('  PERFORMING SELF-TEST\n')
    #time.sleep(0.1)
    print('  SYSTEM DETECTED CONFIGURATION:')
    time.sleep(0.3)
    print('  PROCESSOR: ROBCO PROCESSOR MODULE @ 6MHz')
    time.sleep(0.3)
    print('  MEMORY BANKS AVAILABLE: 2')
    print('  MEMORY BANKS DETECTED: 1')
    print('  MEMORY DETECTED: 256kB')
    time.sleep(0.3)
    print('  STORAGE BUS #1: PAGMEM HARDDISK')
    print('  STORAGE BUS #2: ROBCO (TM) HOLODRIVE')
    print('  STORAGE BUS #3: NULL')
    time.sleep(0.3)
    print('  CONNECTIVITY BUS #1: ROBCO STANDARD MODEM')
    time.sleep(0.3)
    print('  HARDWARE OK')
    
#================================================================================

class termlink(cmd.Cmd):
    intro = '  WELCOME TO ROBCO INDUSTRIES (TM) TERMLINK\n\n  ERROR 0x03C663B3\n  "Network Connection Not Found."\n  "Local Mode Inactive."\n  _______________________________'
    prompt = '  > '
    
    def default(self, line):
        print('SYSTEM ERROR!')
    
    def do_shutdown(self, line):
        quit()
    
    def do_help(self, line):
        print('SYSTEM ERROR!')
        
    def do_set(self, arg):
        if arg == 'term/diag true':
            termlinkTest()
        elif arg == 'term/local true':
            print('  LOCAL MODE ACTIVE')
            currentTermlink = False
            time.sleep(0.4)
            ClearScreen()
            Menu().cmdloop()
        elif arg == 'term/admin true':
            print(' ADMINISTRATOR MODE ACTIVE')
            time.sleep(0.4)
            ClearScreen()
            AdminMenu().cmdloop()
        else:
            print('SYSTEM ERROR!')

class JournalIt(cmd.Cmd):
    prompt = currentPrompt

    fileLoaded = False
    fileWritten = False
    textBuffer = []
    filename = ''
    currentLine = 0

    def default(self, line):
        print('Syntax error.\n')
    
    def preloop(self):
        self.prompt = currentPrompt
        if os.path.exists(directoryRobco + '/journalit') == False:
            os.mkdir(directoryRobco + '/journalit')
            handle = open(directoryRobco + '/journalit/Thank you', 'w')
            handle.write('Thank you for purchasing the PagSoft LLC Journal-It note system.\n')
            handle.write('If you have any problems with our software, please consult the manual or contact your system administrator.')
            handle.close()
            Header()
            print('JOURNAL-IT SOFTWARE (C)PAGSOFT 2072-2077'.center(80))
            print('-'*80)
            print('Journal-It detected new installation. Generating first-use configuration.')
            print('PagMem HardDisk detected. Using for storage.')
            DelayedPrint('. . . . . . . .', 0.1)
            input('Press ENTER to continue...')
            ClearScreen()
            print('JOURNAL-IT SOFTWARE (C)PAGSOFT 2072-2077'.center(80))
            print('-'*80)
        else:
            Header()
            print('JOURNAL-IT SOFTWARE (C)PAGSOFT 2072-2077'.center(80))
            print('-'*80)

    def precmd(self, line):
        ClearScreen()
        Header()
        print('JOURNAL-IT SOFTWARE (C)PAGSOFT 2072-2077'.center(80))
        print('-'*80)
        return cmd.Cmd.precmd(self, line)

    def do_quit(self, line):
        "Quit program."
        if JournalIt.fileLoaded == True and JournalIt.fileWritten == False:
            while True:
                choice = input('Do you want to quit without saving changes [y/n]?')
                if choice.lowercase() == 'y':
                    ClearScreen()
                    sub_cmd = Menu()
                    sub_cmd.cmdloop()
                elif choice.lowercase() == 'n':
                    JournalIt().cmdloop()
        else:
            ClearScreen()
            sub_cmd = Menu()
            sub_cmd.cmdloop()

    def do_list(self, line):
        "List all the files in database."
        filelist = ''
        for entry in os.listdir(directoryRobco + '/journalit'): 
            #if not entry.startswith('.') and entry.is_file():
            filelist = filelist + entry + '; '

        for line in textwrap.wrap(filelist, 80):
            print(line)

    def do_open(self, arg):
        "Open existing file or create new one. Usage: 'open name'"
        f = open(directoryRobco + '/journalit/' + arg, 'w+')
        JournalIt.textBuffer = f.read()
        print("File loaded.")
        JournalIt.fileLoaded = True
        JournalIt.filename = arg

    def do_write(self, line):
        "Write changes to currently opened file."
        if JournalIt.fileLoaded:
            FILE = open(directoryRobco + '/journalit/' + JournalIt.filename, 'w')
            toWriteBuf = ""
            
            for i in range(0, len(JournalIt.textBuffer)):
                toWriteBuf += str(JournalIt.textBuffer[i])
                if i == len(JournalIt.textBuffer) - 1:
                    break
                toWriteBuf += "\n"
                
            FILE.write(toWriteBuf)
            FILE.close()
            JournalIt.fileWritten = True

    def do_status(self, line):
        "Displays program status."
        if JournalIt.fileLoaded:
            print('Currently loaded file: {}'.format(JournalIt.filename))
            print('Current line: {}'.format(JournalIt.currentLine))
            print('Lines in buffer: {}'.format(len(JournalIt.textBuffer)))
        else:
            print('No file is currently loaded.')

    def do_input(self, line):
        "Enter input mode."
        if len(JournalIt.textBuffer[JournalIt.currentLine]) > 0 and JournalIt.currentLine != 0:
            print('Current text in line {}: {}'.format(JournalIt.currentLine, JournalIt.textBuffer[JournalIt.currentLine]))
        line = input('::')
        JournalIt.textBuffer[JournalIt.currentLine] += str(line)
        
    def do_line(self, number):
        "Change currently edited line. Usage: line <number>"
        number = JournalIt.currentLine
        print('Successfully change line to {}'.format(number))

    def do_read(self, line):
        "Read current file."
        if JournalIt.fileLoaded:
            for line in textwrap.wrap(JournalIt.textBuffer, 80):
                if JournalIt.textBuffer > 13:
                    n = 0
                    for i in range(len(JournalIt.textBuffer)):
                        if n < 13:
                            print(line)
                            n = n + 1
                        else:
                            input('File too long. Pres ENTER to move to the next page.')
                            n = 0
                            print(line)
                else:
                    print(line)
                        
        else:
            print('No file is currently loaded.')

class PagMemUtility(cmd.Cmd):
    prompt = currentPrompt
    
    def default(self, line):
        print('Syntax error. Consult the manual.\n')

    def preloop(self):
        self.prompt = currentPrompt
        Header()
        print('PagMem LLC Disk Utility'.center(80))
        print('-'*80)

    def precmd(self, line):
        ClearScreen()
        Header()
        print('PagMem LLC Disk Utility'.center(80))
        print('-'*80)
        return cmd.Cmd.precmd(self, line)

    def do_quit(self, line):
        "Quit program."
        ClearScreen()
        sub_cmd = Menu()
        sub_cmd.cmdloop()

    def do_hardware(self, line):
        "Display information about installed storage hardware."
        print('Bus: 3E2A')
        print('Device type: PagMem HardDisk')
        print('Capacity: 10000000 Bytes')
        print('Serial number: 1337-C271-D4')
        print('\n')
        print('Bus: 3E2B')
        print('Device type: Robco (TM) Holodisk Drive')
        print('Capacity: N/N Bytes')
        print('Serial number: 420D-F128-E2')
        print('\n')
        print('Bus: 3E2C')
        print('NULL')
        
    def do_format(self, line):
        "Format PagMem HardDisk"
        while True:
            choice = input('Do you really want to format your HardDisk? [y/n]')
            if choice.lower() == 'y':
                import shutil
                shutil.rmtree(directoryRobco)
                os.mkdir(directoryRobco)
            elif choice.lower() == 'n':
                break

class SettingsSubmenu(cmd.Cmd):
    prompt = currentPrompt

    def preloop(self):
        self.prompt = currentPrompt
        Header()
        print('RobcOS Personalization Utility for Local Terminals'.center(80))
        print('-'*80)

    def precmd(self, line):
        ClearScreen()
        Header()
        print('RobcOS Personalization Utility for Local Terminals'.center(80))
        print('-'*80)
        return cmd.Cmd.precmd(self, line)
    
    def default(self, line):
        print('Unrecognized command. Try \'help\' for guidance.\n')

    def do_help(self, line):
        print('This utility helps users in personalization of their Robco Personal Terminal.')
        print('Available commands:')
        print('prompt - select prompt used by RobcOS applications.')
        print('username - sets the User Name')
        print('termlink - enter the Termlink Error Resolving Console')
        print('ver - check version of this utility')
        print('quit - quits the application')
        
    def do_ver(self, line):
        "Check RobcOS Personalization Utility version"
        print('RobcOS Personalization Utility 1.04 for Local Terminals')
        print('For RobcOS v.85 Operating System')
        print('(c) 2076 Robco Industries')
        
    def do_username(self, line):
        "Change RobcOS User Name"
        global currentUser
        print('This will change the User Name. Leave blank if you don\'t want to do this.')
        while True:
            choice = str(input(currentPrompt))
            if len(choice) == 0:
                break
            elif len(choice) > 8:
                print('Maximum user name lenght is 8 characters!')
            else:
                currentUser = choice
                print('New user name set.')
                break
        
    def do_prompt(self, line):
        "Select the prompt used in RobcOS applications."
        global currentPrompt
        print('Select the prompt used in RobcOS applications:')
        print('1. ">"')
        print('2. "?"')
        print('3. "~"')
        print('4. ":"')
        print('5. "@"')
        choice = str(input(currentPrompt))
        if choice == '1':
            currentPrompt = '> '
            self.prompt = currentPrompt
            self.precmd(line)
        elif choice == '2':
            currentPrompt = '? '
            self.prompt = currentPrompt
            self.precmd(line)
        elif choice == '3':
            currentPrompt = '~ '
            self.prompt = currentPrompt
            self.precmd(line)
        elif choice == '4':
            currentPrompt = ': '
            self.prompt = currentPrompt
            self.precmd(line)
        elif choice == '5':
            currentPrompt = '@ '
            self.prompt = currentPrompt
            self.precmd(line)
        else:
            print('Error! Selection not recognized. Returning to main screen.')
            self.precmd(line)

    def do_quit(self, line):
        "Quit program."
        ClearScreen()
        sub_cmd = Menu()
        sub_cmd.cmdloop()
    
    def do_termlink(self, line):
        while True:
            choice = input('Do you really want to restart into Termlink Error Resolving Console? [y/n]')
            if choice.lower() == 'y':
                currentTermlink = True
                writeConfig()
                print('Restarting...')
                time.sleep(0.4)
                termlink().cmdloop()
            elif choice.lower() == 'n':
                break
        
class AdminMenu(cmd.Cmd):
    prompt = '> '

    def preloop(self):
        Header()
        print('Administrator Mode'.center(80))
        print('-'*80)

    def precmd(self, line):
        ClearScreen()
        Header()
        print('Administrator Mode'.center(80))
        print('-'*80)
        return cmd.Cmd.precmd(self, line)

    def do_install(self, line):
        "Tries to install program from HoloDisk"
        ClearScren()
        print("ERROR! Holodisk not found! Put the Holodisk into the Holodisk Drive and try again.")
        
    def do_help(self, line):
        print("install - starts the installation of the software on the Holodisk")
        print("shutdown - shuts the Terminal down")

    
    def do_shutdown(self, line):
        "Safely shuts the terminal down"
        while True:
            choice = input('Do you really want to shut down? [y/n]')
            if choice.lower() == 'y':
                writeConfig()
                quit()
            elif choice.lower() == 'n':
                AdminMenu().cmdloop()
        
class HistorySubmenu(cmd.Cmd):
    prompt = currentPrompt

    def preloop(self):
        self.prompt = currentPrompt
        Header()
        print('Welcome to RobCo Self Teaching System: History Edition!\nPlease enter command or type help to list available commands.'.center(80))
        print('-'*80)

    def precmd(self, line):
        ClearScreen()
        Header()
        print('Welcome to RobCo Self Teaching System: History Edition!\nPlease enter command or type help to list available commands.'.center(80))
        print('-'*80)
        return cmd.Cmd.precmd(self, line)

    def do_constitution(self, line):
        "Constitution of The United States of America"

    def do_quit(self, line):
        "Quit program."
        ClearScreen()
        sub_cmd = Menu()
        sub_cmd.cmdloop()
        
#================================
        
class Menu(cmd.Cmd):
    prompt = currentPrompt
    
    def default(self, line):
        print('Unrecognized command. Try \'help\' for guidance.\n')

    def preloop(self):
        self.prompt = currentPrompt
        Header()
        print('Welcome {}! Please enter command or type help to list available programs.'.format(currentUser).center(80))
        print('-'*80)

    def precmd(self, line):
        ClearScreen()
        Header()
        print('Welcome {}! Please enter command or type help to list available programs.'.format(currentUser).center(80))
        print('-'*80)
        return cmd.Cmd.precmd(self, line)

    def do_help(self, line):
        print('Internal System Commands:')
        print('calendar [year] [month] - displays calendar for given month')
        print('date - displays current system date')
        print('settings - RobcOS Personalization Utility')
        print('shutdown - safely shuts down')
        print('time - displays current system time')
        print('ver - displays RobcOS version.')
        print('\nInstalled software:')
        print('diskutil - PagMem LLC Disk Utility')
        print('journalit - PagSoft Journal-It')
        print('grelok - Reign of Grelok (beta v.632) game')

    def do_shutdown(self, line):
        "Safely shuts the terminal down"
        while True:
            choice = input('Do you really want to shut down? [y/n]')
            if choice.lower() == 'y':
                writeConfig()
                quit()
            elif choice.lower() == 'n':
                Menu().cmdloop()
                
    def do_settings(self, line):
        "RobcOS Personalization Utility"
        ClearScreen()
        sub_cmd = SettingsSubmenu()
        sub_cmd.cmdloop()
                
    def do_time(self, line):
        "Display system time"
        print(time.strftime("%H:%M", time.localtime()))
        
    def do_calendar(self, arg):
        import calendar
        #c = calendar.TextCalendar()
        #c.prmonth(year, month)
        if len(arg.split()) == 2:
            try:
                year = int(arg.split()[0])
                month = int(arg.split()[1])
                calendar.TextCalendar().prmonth(year, month)
            except ValueError:
                print('You must provide numbers!')
        elif len(arg.split()) == 0:
            calendar.TextCalendar().prmonth(int(time.strftime('%Y')), int(time.strftime('%m')))
        else:
            print('Usage: calendar [year] [month]. \nYear and month must be given in the following format: 2016 3. \nOmitting them displays current month.')
        
    def do_date(self, line):
        "Display system date"
        print(time.strftime("%a, %d-%m-%Y", time.localtime()))

    def do_ver(self, line):
        "Displays system version"
        print('RobcOS v.85 (C) 2076 Robco Industries')
        print('UnifiedOS Desktop System {}'.format(__version__))

    def do_diskutil(self, line):
        "PagMem LLC Disk Utility"
        ClearScreen()
        sub_cmd = PagMemUtility()
        sub_cmd.cmdloop()

    def do_journalit(self, line):
        "PagSoft Journal-It software"
        ClearScreen()
        sub_cmd = JournalIt()
        sub_cmd.cmdloop()

    def do_grelok(self, line):
        "Reign of Grelok (beta v.632)"
        ClearScreen()
        grelok.GrelokNew()

#===========================================================================

if __name__ == '__main__':	
    if len(sys.argv) == 1:
        currentPrompt = readConfig()[0]
        currentUser = readConfig()[1]
        currentTermlink = readConfig()[2]
        #Start()
        ClearScreen()
        if currentTermlink == True:
            termlink().cmdloop()
        else:
            Menu().cmdloop()
    else:
        if sys.argv[1] ==  '-v' or '--ver':
            print("pyRobco3 {}".format(__version__))
        elif sys.argv[1] == '--help' or '-h':
            print('''Usage: 
 Without any parameter - normal start
 -v or --ver - display version information, then exit
 -h or --help - display the help without starting
 --easy - start in easy mode (without Termlink)''') 
        elif sys.argv[1] == '--easy':
            currentPrompt = readConfig()[0]
            currentUser = readConfig()[1]
            currentTermlink = readCOnfig()[2]
            Start()
        else:
            print('Use -h switch to see help')
